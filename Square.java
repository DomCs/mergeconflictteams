public class Square {
    private double length;
    private double width;

    public Square(double length, double width){
        this.length = length;
        this.width = width;
    }

    //Get Methods
    public double getLength(){
        return this.length;
    }
    public double getWidth(){
        return this.width;
    }

    //Getting the Area
    public double getArea(){
        return this.length * this.width;
    }

    //Printing out the Square
    public String toString(){
        return "Length: "+this.length+", Width: "+this.width;
    }
}
