public class Shapes{
	public static void main(String[] arg){
		Triangle tri = new Triangle(3, 5);

		System.out.println(tri);
		System.out.println("Triangle area: " + tri.getArea());
		
		Square sqr = new Square(3,5);

		System.out.println(sqr);
		System.out.println("Area: "+sqr.getArea());
	}

}

