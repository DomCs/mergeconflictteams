
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TriangleTest {

    // this test will fail
    @Test
    public void testGetMethods() {
        //fail("this test will fail");
        Triangle tri = new Triangle(3, 5);
        assertEquals(tri.getBase(),3);
        assertEquals(tri.getHeight(),5);
    }

    @Test
    // this test will succeed. In practice we would put
    // a real method call into the assertEquals
    public void testGetArea() {
        //assertEquals(1,1);
        Triangle tri = new Triangle(3, 5);
        assertEquals(tri.getArea(),7.5);
    }

    @Test
    public void testToString() {
        //fail("this test will fail");
        Triangle tri = new Triangle(3, 5);
        assertEquals(tri.toString(),"Base: 3.0, Height: 5.0");
    }
}

